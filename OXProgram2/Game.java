import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player('O');
		x = new Player('X');
		

	}

	public void play() {
	while(true) {
		playOne();
	}

	}
	public void playOne() {
		board = new Board(x, o);
		showWelcome();
		while (true) {
			showTable();
			showTurn();
			input();
			if(board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		showTable();
		showWinner();
		showStat();
	}
private void showStat() {
		System.out.println(x.getName()+" W,D,L:"
				+ x.getWin()+","
				+x.getDraw()+","
				+x.getLose()
				);
		System.out.println(o.getName()+" W,D,L:"
				+ o.getWin()+","
				+o.getDraw()+","
				+o.getLose()
				);
		
	}

private void showWinner() {
	Player player = board.getWinner();
	System.out.println(player.getName()+" win....");
}
	public void showWelcome() {
		System.out.println("Welcome to OX Game.");
	}

	public void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print((i + 1));
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}
	}

	private void showTurn() {
		Player player = board.getCurrent();
		System.out.println(player.getName() + " turn..");
	}

	private void input() {
		Scanner sc = new Scanner(System.in);
		while (true) {
			try {
				System.out.println("Please input Row Col: ");
				String input = sc.nextLine();
				String[] str = input.split(" ");
				if (str.length != 2) {
					System.out.println("Please input Rol Col (ex: 1 2");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if (board.setTable(row, col) == false) {
					System.out.println("Table is not empty!!");
					continue;
				}
				break;

			} catch (Exception e) {
				System.out.println("Please input Rol Col (ex: 1 2");
				continue;
			}
		}

	}
}
